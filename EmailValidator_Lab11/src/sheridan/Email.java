package sheridan;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Email {
	
	private final static Logger LOGGER = 
            Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	// 1. Email must be in this format: <account>@<domain>.<extension>

	public static boolean isValidEmail(String email) {

		String pattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		return email.matches(pattern);

	}

	// 2. Email should have one and only one @ symbol.

	public static boolean isValidCharEmail(String email) {

		String pattern = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+(?:\\.[a-zA-Z0-9_!#$%&'*+/=?`{|}~^-]+)*@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		return email.matches(pattern);

	}

	// 3. Account name should have at least 3 alpha-characters in lowercase (must
	// not start with a number).
	public static boolean checkLowercaseAccountNameAndNotBeginWithDigit(String email) {

		String account = email.substring(0, email.indexOf("@"));

		int count = 0;

		boolean checkCount = false;
		for (char c : account.toCharArray()) {
			if (Character.isLowerCase(c)) {
				count++;
			}
			if (count == 3) {
				checkCount = true;
				break;
			}
		}

		return checkCount && Character.isLetter(account.charAt(0));
	}

	// 4. Domain name should have at least 3 alpha-characters in lowercase or
	// numbers.
	public static boolean checkLowerCaseDomainName(String email) {
		String domainName = email.substring(email.indexOf("@") + 1, email.indexOf("."));
		
		int count = 0;
		boolean checkCount = false;
		for (char c : domainName.toCharArray()) {
			if (Character.isLowerCase(c) || Character.isDigit(c)) {
				count++;
			}
			if (count == 3) {
				checkCount = true;
				break;
			}
		}
		return checkCount;
	}

	// 5.Extension name should have at least 2 alpha-characters (no numbers).
	public static boolean checkLowerCaseExtensionName(String email) {
		String extensionName = email.substring(email.indexOf(".") + 1);

		int count = 0;
		boolean checkCountAndNumber = false;
		for (char c : extensionName.toCharArray()) {
			if (Character.isDigit(c)) {
				return false;
			}
			if (Character.isLetter(c)) {
				count++;
			}
			if (count == 2) {
				checkCountAndNumber = true;

			}
		}

		return checkCountAndNumber;
	}
	
	
	public static void main(String args[]) {
		
    LOGGER.log(Level.INFO, "Email Validator 1.0");
    String email = "sarthakdhingra@sheridancollege.ca";
    LOGGER.log(Level.INFO, "Email generated for testing: "+ email);
    boolean result=(isValidEmail(email) && isValidCharEmail(email) && checkLowercaseAccountNameAndNotBeginWithDigit(email) && checkLowerCaseDomainName(email) && checkLowerCaseExtensionName(email) ) ;
    LOGGER.log(Level.INFO, "Is email valid:? :"+ result);
    
    
	
	}

}
