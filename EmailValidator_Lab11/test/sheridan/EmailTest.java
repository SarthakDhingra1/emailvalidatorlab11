package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailTest {

	// 1. Email must be in this format: <account>@<domain>.<extension>
	// Regular
	@Test
	public void testEmailFormatRegular() {

		assertTrue(Email.isValidEmail("sarthak@gmail.com"));
	}

	// Exception
	@Test
	public void testEmailFormatException() {

		assertFalse("Invalid Email chars", Email.isValidEmail("@gmail..com"));
	}

	// Boundary-In
	@Test
	public void testEmailFormatBoundaryIn() {

		assertTrue(Email.isValidEmail("s@g.c"));
	}

	// Boundary-Out
	@Test
	public void testEmailFormatBoundaryOut() {

		assertFalse("Invalid Email chars", Email.isValidEmail("sarthak.com"));
	}

	// 2. Email should have one and only one @ symbol.
	// Regular
	@Test
	public void testCharEmailRegular() {

		assertTrue(Email.isValidCharEmail("sarthak@gmail.com"));
	}

	// Exception
	@Test
	public void testCharEmailException() {

		assertFalse("Invalid Email chars", Email.isValidCharEmail("sarthak@@dhingra.com"));
	}

	// 3. Account name should have at least 3 alpha-characters in lowercase (must
	// not start with a number).
	// Regular
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitRegular() {

		assertTrue(Email.checkLowercaseAccountNameAndNotBeginWithDigit("sarthak@gmail.com"));
	}

	// Exception
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitExceptionDigit() {

		assertFalse("Account name cannot begin with number",
				Email.checkLowercaseAccountNameAndNotBeginWithDigit("1d@gmail.com"));
	}

	// Exception
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitExceptionCount() {

		assertFalse("Account name cannot be less than 3",
				Email.checkLowercaseAccountNameAndNotBeginWithDigit("@gmail.com"));
	}

	// Exception
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitExceptionCase() {

		assertFalse("Account name cannot have uppercase letters",
				Email.checkLowercaseAccountNameAndNotBeginWithDigit("SD@gmail.com"));
	}

	// Boundary-In
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitBoundaryIn() {

		assertTrue(Email.checkLowercaseAccountNameAndNotBeginWithDigit("sdp@gmail.com"));
	}

	// Boundary-Out
	@Test
	public void testcheckLowercaseAccountNameAndNotBeginWithDigitBoundaryOut() {

		assertFalse("Account name cannot be less than 3",
				Email.checkLowercaseAccountNameAndNotBeginWithDigit("sd@gmail.com"));
	}

	// 4. Domain name should have at least 3 alpha-characters in lowercase or
	// numbers.
	// Regular
	@Test
	public void testcheckLowerCaseDomainNameRegular1() {

		assertTrue(Email.checkLowerCaseDomainName("sd@abcdef.com"));
	}

	// Regular
	@Test
	public void testcheckLowerCaseDomainNameRegular2() {

		assertTrue(Email.checkLowerCaseDomainName("sd@12345.com"));
	}

	// Regular
	@Test
	public void testcheckLowerCaseDomainNameRegular3() {

		assertTrue(Email.checkLowerCaseDomainName("sd@ab1.com"));
	}

	// Exception
	@Test
	public void testcheckLowerCaseDomainNameExceptionCount() {

		assertFalse("Domain name cannot be less than 3", Email.checkLowerCaseDomainName("sd@ab.com"));
	}

	// Exception
	@Test
	public void testcheckLowerCaseDomainNameExceptionCase() {

		assertFalse("Domain name cannot have uppercase characters", Email.checkLowerCaseDomainName("sd@ABCD.com"));
	}

	// Boundary-Out
	@Test
	public void testcheckLowerCaseDomainNameBoundaryOut() {

		assertFalse("Domain name cannot be less than 3", Email.checkLowerCaseDomainName("sd@sd.com"));
	}

	// Boundary-In
	@Test
	public void testcheckLowerCaseDomainNameBoundaryIn() {

		assertTrue(Email.checkLowerCaseDomainName("sd@sd1.com"));
	}

	// 5.Extension name should have at least 2 alpha-characters (no numbers).
	// Regular
	@Test
	public void testCheckLowerCaseExtensionNameRegular() {

		assertTrue(Email.checkLowerCaseExtensionName("sd@gmail.com"));
	}

	// Exception
	@Test
	public void testCheckLowerCaseExtensionNameException() {

		assertFalse("Extension cannot have numbers", Email.checkLowerCaseExtensionName("sd@gmail.co1"));
	}

	// Boundary-In
	@Test
	public void testCheckLowerCaseExtensionNameBoundaryIn() {

		assertTrue(Email.checkLowerCaseExtensionName("sd@gmail.co"));
	}

	// Boundary-Out
	@Test
	public void testCheckLowerCaseExtensionNameBoundaryOut() {

		assertFalse("Extension cannot be less than 2 characters", Email.checkLowerCaseExtensionName("sd@gmail.c"));
	}
}
